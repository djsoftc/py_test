# -*- coding: utf-8 -*-
import datetime
import time
import pandas as pd
import numpy as np
from pandas.tseries.offsets import DateOffset
import sys
# x=[[2,3],[3,4],[1,2],[4,5],[5,6],[6,7],[7,8],[8,9],[33,50],[44,66]]
# y=[[8],[18],[5],[23],[28],[33],[38],[43],[216],[286]]
#
# import statsmodels.api as sm
# wls_model = sm.WLS(y, x).fit()
# print wls_model.summary()
import traceback

def str2datetime():
    date_str = '2015-08-28'
    format_type = "%Y-%m-%d"

    timeArray = time.strptime(date_str, format_type)
    timeStamp = int(time.mktime(timeArray))
    print timeArray,timeStamp

def cancat_test():
    frame = pd.DataFrame(np.random.randn(4, 3), columns=list('abc'))
    # frame1 = pd.DataFrame(np.random.randn(1, 3), columns=list('d'))
    print frame
    for i,data in frame.iterrows():
        print i,data
    for i,data in enumerate(frame.values):
        print i,data

def list_test():
    list = []
    list01 = [1,2,3]
    list02 = [3,4,5]
    list.extend(list01)
    list.extend(list02)
    print list

def list_test02():
    list01 = [1,2,3]
    list02 = [3,4,5]
    list03 = list01 + ['c']
    print list01
    print list03

def series_test():
    ser_obj = None
    ser_obj01 = pd.Series(range(0,4))
    ser_obj = ser_obj01
    ser_obj02 = pd.Series(range(3,6))
    ser_obj = ser_obj.append(ser_obj02)
    print ser_obj

def get_date_range(start_date,end_date,freq_num=1):
    freq=DateOffset(months=freq_num)

    time_obj = pd.date_range(start=start_date,end=end_date,freq=freq)
    df_obj = pd.DataFrame(data=time_obj,columns=['start'])
    df_obj['end'] = df_obj['start']+ DateOffset(months=12)- DateOffset(days=1)
    df_obj['test_start'] = df_obj['end']+ DateOffset(days=1)
    df_obj['test_end'] = df_obj['end'] + DateOffset(months=1)

    df_obj = df_obj[df_obj['end'] <= end_date]
    df_obj = df_obj[df_obj['test_start'] < end_date]
    if (df_obj['end'] < end_date).size == 1 and (df_obj['end'] < end_date)[0]:
        df_obj['start'] = pd.to_datetime(start_date)
        df_obj['end'] = pd.to_datetime(end_date)

    df_obj['test_end'][df_obj.shape[0] - 1] = pd.to_datetime(end_date)
    return df_obj

def df_test():
    frame = pd.DataFrame(np.random.randn(4, 3), columns=list('abc'))
    print frame[['a','b']]

def df_sort():
    frame = pd.DataFrame(np.random.randn(4, 3), columns=list('bac'))
    print frame
    frame.sort_values(by=0,axis=1,inplace=True)
    print frame

def limit_df():
    alpha_list = ['a','b','c']
    df_obj = pd.DataFrame(data=alpha_list,columns=['facotr'])
    df_obj['lower_limit'] = 10.0
    df_obj['upper_limit'] = 10.0

    alpha_limit_list = []
    for index, row in df_obj.iterrows():
        alpha_limit_list.append(row.to_dict())

    print alpha_limit_list

    df_limit = pd.DataFrame.from_dict(alpha_limit_list)
    print df_limit

def alpha_connection():
    url = 'sqlite:///' + sys.path[4] + '/applications/pycube/databases/storage.sqlite'
    url2= 'sqlite:////www/app/py_cube/web2py/applications/pycube/databases/storage.sqlite'
    # 'sqlite:////home/hanyx/anaconda2/lib/python27.zip/applications/pycube/databases/storage.sqlite'
    # 'sqlite:////www/app/py_cube/web2py/applications/pycube/databases/storage.sqlite'
    print url,url2
    connection = create_engine(url2)
    return connection

from sqlalchemy import create_engine
def read_csv_alpha_stock_data():
    style_factors_pd = pd.read_csv('/home/hanyx/文档/style_factor_update_1208.csv')
    style_factors_pd = style_factors_pd[['tradecode', 'tradedate', 'rets', 'Y_rets', 'risk_free', 'volume', 'close', 'mkt_cap_ard_org', 'Beta', 'Momentum','Size', 'Earn_Yiled', 'Volatility', 'Growth', 'Value', 'Leverage', 'Liquidity']]
    print style_factors_pd.head()
    print style_factors_pd.shape
    connect = alpha_connection()
    style_factors_pd.to_sql("alpha_stock_data",connect,if_exists="replace",index=False,chunksize=10000)

def read_csv_alpha_hs300_weight():
    style_factors_pd = pd.read_csv('/home/hanyx/文档/HS300_weight2005-04-08-2016-12-31.csv',encoding='utf-8')
    style_factors_pd.columns = ['id','date','tradecode','weight']
    style_factors_pd = style_factors_pd[['date','tradecode','weight']]
    print style_factors_pd.head()
    print style_factors_pd.shape
    style_factors_pd['date'] = pd.to_datetime(style_factors_pd['date'])
    style_factors_pd['date'] = style_factors_pd['date'].apply(lambda x:x._date_repr)
    style_factors_pd.to_sql("alpha_hs300_weight",alpha_connection(),if_exists="replace",index=False,chunksize=10000)

def read_csv_stockindustry():
    style_factors_pd = pd.read_csv('/home/hanyx/文档/indurstry.csv',encoding='utf-8')
    style_factors_pd.columns = ['id','industry','tradecode']
    style_factors_pd = style_factors_pd[['industry','tradecode']]
    print style_factors_pd.head()
    print style_factors_pd.shape
    style_factors_pd.to_sql("stockindustry",alpha_connection(),if_exists="replace",index=False,chunksize=10000)

def read_csv_alpha_hs300_weight():
    style_factors_pd = pd.read_csv('/home/hanyx/文档/HS300_df.csv',encoding='utf-8')
    style_factors_pd.columns = ['tradedate','tradecode','close','risk_free','rets','rf_rets']

    style_factors_pd = style_factors_pd[['tradedate','rets','risk_free','rf_rets']]
    print style_factors_pd.head()
    print style_factors_pd.shape
    style_factors_pd['tradedate'] = pd.to_datetime(style_factors_pd['tradedate'])
    style_factors_pd['tradedate'] = style_factors_pd['tradedate'].apply(lambda x:x._date_repr)
    style_factors_pd.to_sql("alpha_hs300_data",alpha_connection(),if_exists="replace",index=False,chunksize=10000)

import scipy.stats as stats
import scipy.optimize as opt
import scipy

def func(x, sign=1.0):
    """ Objective function """
    return sign*(2*x[0]*x[1] + 2*x[0] - x[0]**2 - 2*x[1]**2)

def func_deriv(x, sign=1.0):
    """ Derivative of objective function """
    dfdx0 = sign*(-2*x[0] + 2*x[1] + 2)
    dfdx1 = sign*(2*x[0] - 4*x[1])
    return np.array([ dfdx0, dfdx1 ])

def func_optimize():
    # x_0 = np.array([0.5, 1.6, 1.1, 0.8, 1.2])
    # res = opt.minimize(rosen, x_0, method='nelder-mead', options={'xtol': 1e-8, 'disp': True})
    # print "Result of minimizing Rosenbrock function via Nelder-Mead Simplex algorithm:"
    # print res
    pass


def df_append():
    df_obj_01 = pd.DataFrame(data=np.random.rand(2,3),columns=[u'00001.SZ',u'00002.SZ',u'00003.SZ'])
    df_obj_02 = pd.DataFrame(data=np.random.rand(1,3), columns=[u'00001.SZ',u'00002.SZ',u'00005.SZ'])
    print df_obj_01
    print df_obj_02
    print df_obj_01.append(df_obj_02)


def df_del_null():
    df_obj_01 = pd.DataFrame(data=np.random.rand(2, 3), columns=['a', 'b', 'c'])
    df_obj_02 = pd.DataFrame(data=np.random.rand(2, 3), columns=['a', 'b', 'd'])
    df_obj_03 = df_obj_01.append(df_obj_02)
    df_obj_03['e'] = np.nan
    print df_obj_03

    print df_obj_03.dropna(axis=1,how='all')

def get_date_range02(start_date,end_date,freq_num=1):
    freq = DateOffset(months=freq_num)

    time_obj = pd.date_range(start=start_date, end=end_date, freq=freq)
    df_obj = pd.DataFrame(data=time_obj, columns=['start'])
    df_obj['end'] = df_obj['start'] + DateOffset(months=12) - DateOffset(days=1)
    df_obj['test_start'] = df_obj['end'] + DateOffset(days=1)
    df_obj['test_end'] = df_obj['test_start'] + DateOffset(months=1) - DateOffset(days=1)
    df_obj = df_obj[df_obj['end'] <= end_date]
    df_obj = df_obj[df_obj['test_start'] < end_date]
    if (df_obj['end'] < end_date).size == 1 and (df_obj['end'] < end_date)[0]:
        df_obj['start'] = pd.to_datetime(start_date)
        df_obj['end'] = pd.to_datetime(end_date)

    df_obj['test_end'][df_obj.shape[0] - 1] = pd.to_datetime(end_date)
    if df_obj.size == 0:
        columns = df_obj.columns
        start = pd.to_datetime(start_date)
        end = pd.to_datetime(end_date)
        test_start = end + DateOffset(days=1)
        test_end = test_start + DateOffset(months=1)
        df_obj = pd.DataFrame(data=[start, end, test_start, test_end]).T
        df_obj.columns = columns
    return df_obj

def get_date_range(start_date,end_date,freq_num=1):
    freq = DateOffset(months=freq_num)

    time_obj = pd.date_range(start=start_date, end=end_date, freq=freq)
    df_obj = pd.DataFrame(data=time_obj, columns=['start'])
    df_obj['end'] = df_obj['start'] + DateOffset(months=12)
    df_obj = df_obj[df_obj['end'] <= end_date]
    df_obj['end'][df_obj.shape[0] - 1] = pd.to_datetime(end_date)
    if df_obj.size != 0:
        df_obj['test_start'] = df_obj['end'] + DateOffset(days=1)
        df_obj['test_end'] = df_obj['test_start'] + DateOffset(months=1) - DateOffset(days=1)
    else:
        start = pd.to_datetime(start_date)
        end = pd.to_datetime(end_date)
        test_start = end + DateOffset(days=1)
        test_end = test_start + DateOffset(months=1)

        df_obj['start'] = [start]
        df_obj['end'] = end
        df_obj['test_start'] = test_start
        df_obj['test_end'] = test_end
    return df_obj

def np_del():
    a = np.array(range(3))
    print a
    a = np.delete(a, 1, axis=0)
    print a
    a = np.delete(a, 1, axis=0)
    print a

def list_extend():
    list = [1,2,3]
    list.extend([4])

    it = iter(list)
    for i in range(len(list)):
        print it.next()

def get_base_data(start_date,end_date):
    """
    根据日期获取策略基本数据
    :param start_date: 起始时间
    :param end_date: 结束时间
    :return: df_obj
    """
    # sql = "select * from alpha_stock_data where tradedate >= '" + start_date + "' and tradedate <= '" + end_date + "'"
    # df_obj = pd.read_sql(sql,alpha_connection(),columns=get_stock_data_column_list())
    # return df_obj
    pass

def multi_run_wrapper(args):
    return add(*args)
def add(x,y,z):
    return x+y+z
def add1(x,y,z):
    return x+y+z

from multiprocessing import Pool
def pool_test():
    pool = Pool(4)
    list = []
    list.append((1, 2,3))
    list.append((2, 3,3))
    list.append((3, 4,3))
    print list
    results = pool.map(multi_run_wrapper, list)
    pool.close()
    pool.join()
    print results

if __name__ == '__main__':
    reload(sys)  # 2
    sys.setdefaultencoding('utf-8')  # 3
    try:
        pool_test()
        print True == False
        # alpha_connection()
        # str2datetime()
        # cancat_test()
        # list_test()
        # series_test()
        # print get_date_range('2014-01-01','2015-02-01')
        # df_test()
        # list_test02()
        # df_sort()
        # limit_df()
        # read_csv_stockindustry()
        # func_optimize()
        # df_append()
        # df_del_null()
        # read_csv_alpha_stock_data()
        # np_del()
        # print get_date_range('2011-01-01','2013-01-01')
        # list_extend()


        # func_optimize()
    except Exception, e:
        print traceback.format_exc()