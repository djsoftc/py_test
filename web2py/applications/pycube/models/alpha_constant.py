# -*- coding: utf-8 -*-

stock_data_column_list = ['tradecode', 'tradedate', 'rets', 'Y_rets', 'risk_free', 'volume', 'close', 'mkt_cap_ard_org', 'Beta','Momentum', 'Size', 'Earn_Yiled', 'Volatility', 'Growth', 'Value', 'Leverage', 'Liquidity']

hs300_data_column_list =  ['tradedate','rets','risk_free','rf_rets']

POOL_SIZE = 3 # 进程池数量
restart_flag = 0 # True是重新执行, 1是从db里得到结果
multi_single_flag = 0 # True多进程, 1是串行

# alpha_policy_json
pure_factor_grids_type = '0'
alpha_limit_list_type = '1'
f_pre_type = '2'