import time
t0 = time.time()
if session.authorized:
    if session.last_time and session.last_time < t0 - auth.settings.expiration:
        session.flash = T('session expired')
        session.authorized = False
    else:
        session.last_time = t0

if not session.authorized  and not \
    (request.controller + '/' + request.function in
     ('pages/login','weixin/index')):
    redirect(URL('pages', 'login'))

response.generic_patterns = ['*'] #if request.is_local else []
is_not_empty = IS_NOT_EMPTY(error_message=auth.messages.is_empty)
comm_fields =[Field('create_by', length=100, default=auth.user_id, writable=False, readable=False,
                               label='创建者'),
                         Field('create_date', 'datetime', default=request.now, writable=False, readable=False,
                               label='创建时间'),
                         Field('update_by', length=100, default=auth.user_id, update=auth.user_id, writable=False,
                               readable=False, label='更新者'),
                         Field('update_date', 'datetime', default=request.now, update=request.now, writable=False,
                               readable=False, label='更新时间'),
                         Field('remarks', length=255, default='', label='备注信息'),
                         Field('del_flag', length=1, default='1',writable=False,readable=False, label='记录状态')]


db.define_table('sys_menu',
                Field('parent_id',length=64, default='',label='父级编号'),
                Field('name',length=100, default='',label='名称',requires=is_not_empty),
                Field('sortno','integer', default=0, label='排序',requires=IS_INT_IN_RANGE(0, 10 ** 3)),
                Field('href', length=2000, default='', label='链接'),
                Field('target', length=100, default='navTab', label='目标',requires=IS_IN_SET(['navTab', 'modal'])),
                Field('icon', length=100, default='', label='图标'),
                Field('is_show','boolean', default=True, label='是否在菜单中显示'),
                Field('permission', length=200, default='', label='权限标识'),
                *comm_fields,
                format='%(name)s (%(id)s)'
                )
db.sys_menu.parent_id.requires = IS_EMPTY_OR(IS_IN_DB(db,db.sys_menu.id, '%(name)s',zero=T( 'choose one' )))

db.define_table('sys_role_menu',
                Field('group_id','reference auth_group' ,label='角色编号'),
                Field('menu_id','reference sys_menu',label='菜单编号'),
                *comm_fields
                )

db.define_table('sys_dict',
                Field('enumvalue',length=100, default='',label='数据值',requires=is_not_empty),
                Field('enumlabel',length=100, default='',label='标签名',requires=is_not_empty),
                Field('enumtype',length=100, default='',label='类型',requires=is_not_empty),
                Field('description',length=100, default='',label='描述',requires=is_not_empty),
                Field('sortno','integer', default=0, label='排序',requires=IS_INT_IN_RANGE(0, 10 ** 3)),
                Field('parent_id', length=64, default='0', label='父级编号'),
                *comm_fields
                )