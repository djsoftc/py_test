is_not_empty = IS_NOT_EMPTY(error_message=auth.messages.is_empty)
comm_fields =[Field('create_by', length=100, default=auth.user_id, writable=False, readable=False,
                               label='创建者'),
                         Field('create_date', 'datetime', default=request.now, writable=False, readable=False,
                               label='创建时间'),
                         Field('update_by', length=100, default=auth.user_id, update=auth.user_id, writable=False,
                               readable=False, label='更新者'),
                         Field('update_date', 'datetime', default=request.now, update=request.now, writable=False,
                               readable=False, label='更新时间'),
                         Field('remarks', length=255, default='', label='备注信息'),
                         Field('del_flag', length=1, default='1',writable=False,readable=False, label='记录状态')]

db.define_table('alpha_factor',
                Field('parent_id',length=64, default='',label='父级编号'),
                Field('name',length=100, default='',label='名称',requires=is_not_empty),
                Field('sortno','integer', default=0, label='排序',requires=IS_INT_IN_RANGE(0, 10 ** 3)),
                Field('memo', length=2000, default='', label='描述'),
                Field('sqlcodes', length=2000, default='', label='sql语句'),
                Field('calc', length=2000, default='', label='计算公式'),
                Field('icon', length=100, default='', label='图标'),
                Field('is_show','boolean', default=True, label='是否有效'),
                Field('permission', length=200, default='', label='权限标识'),
                *comm_fields,
                format='%(name)s (%(id)s)'
                )
db.define_table('alpha_stock_pool',
                Field('stockcode',length=50, default='',label='代码'),
                Field('name',length=100, default='',label='名称'),
                Field('typename',length=8, default='',label='类别'),
                Field('description',length=100, default='',label='描述'),
                *comm_fields,
                format='%(name)s (%(id)s)'
                )
db.define_table('alpha_table',
                Field('tradeDate',length=8, default='',label='交易日期'),
                Field('a1','decimal(12,10)', default=0, label='因子1'),
                Field('a2','decimal(12,10)', default=0, label='因子2'),
                Field('a3','decimal(12,10)', default=0, label='因子3'),
                *comm_fields,
                format='%(name)s (%(id)s)'
                )

