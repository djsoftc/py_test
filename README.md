PyCube是基于WEB2PY（python web框架）的开源开发的，前端采用AdminLTE模板，其核心设计目标是开发迅速、代码量少、学习简单、功能强大、轻量级、易扩展、不用IDE、多语言支持等特点

![输入图片说明](http://git.oschina.net/uploads/images/2017/0103/113419_1f2a121b_579307.png "PyCube登录页面")
![输入图片说明](http://git.oschina.net/uploads/images/2017/0103/113447_81b352e5_579307.png "PyCube首页")


